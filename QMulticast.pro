QT += core gui widgets network

TARGET = QMulticast

CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    qmulticast.cpp \
    qpacketqueue.cpp \
    qudpreceiver.cpp \
    qhttpserver.cpp \
    qpacketdownloader.cpp

HEADERS += qmulticast.h \
    qpacketqueue.h \
    qudpreceiver.h \
    qhttpserver.h \
    qpacketdownloader.h
