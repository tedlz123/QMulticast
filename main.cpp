#include <QApplication>
#include <QThread>
//#include <QTime>
#include "qhttpserver.h"
#include "qmulticast.h"

int main(int argc, char *argv[])
{
    QApplication a(argc,argv);
    a.setQuitOnLastWindowClosed(true);
    QMulticast *multicast=new QMulticast();
    multicast->start();
    qRegisterMetaType<int32_t>("int32_t");
    //QTime t=QTime::currentTime();
    //qsrand(t.hour()*3600000+t.minute()+60000+t.second()*1000+t.msec());
    QHttpServer *httpServer=new QHttpServer();
    QThread *thread=new QThread();
    httpServer->moveToThread(thread);
    QObject::connect(thread,SIGNAL(started()),httpServer,SLOT(restart()));
    thread->start();
    int value=a.exec();
    thread->exit();
    thread->wait();
    httpServer->deleteLater();
    delete thread;
    delete multicast;
    return value;
}
