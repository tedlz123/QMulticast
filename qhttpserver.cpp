#include "qhttpserver.h"

QHttpServer::QHttpServer(QObject *parent) : QObject(parent), tcpServer(NULL), udpReceiver(NULL)
{

}

QHttpServer::~QHttpServer()
{
    if(udpReceiver!=NULL)
    {
        delete udpReceiver;
    }
    if(tcpServer!=NULL)
    {
        tcpServer->close();
        delete tcpServer;
    }
}

void QHttpServer::restart()
{
    mutex.lock();
    qDebug()<<"QHttpServer is restarting.";
    if(udpReceiver!=NULL)
    {
        delete udpReceiver;
        udpReceiver=NULL;
    }
    if(tcpServer!=NULL)
    {
        tcpServer->close();
        delete tcpServer;
    }
    tcpServer=new QTcpServer();
    if(tcpServer->listen(QHostAddress::Any,1759))
    {
        qDebug()<<"Successfully binded to Port 1759.";
    }
    else
    {
        qDebug()<<"Failed to bind QHttpServer.";
    }
    QObject::connect(tcpServer,SIGNAL(newConnection()),this,SLOT(accept()));
    tcpSocket=NULL;
    mutex.unlock();
}

void QHttpServer::accept()
{
    if(this->tcpSocket!=NULL)
    {
        tcpServer->nextPendingConnection()->close();
        return;
    }
    this->tcpSocket=tcpServer->nextPendingConnection();
    QObject::connect(this->tcpSocket,SIGNAL(disconnected()),this,SLOT(restart()));
    this->tcpSocket->waitForReadyRead();
    QByteArray array=this->tcpSocket->readAll();
    int start=array.indexOf("GET /");
    int end=array.indexOf(" HTTP");
    int question=array.indexOf("?");
    QString channelStr;
    if(question<0)
        channelStr=array.right(array.size()-start-5).left(end-start-5);
    else
        channelStr=array.right(array.size()-start-5).left(question-start-5);
    channelStr=channelStr.left(channelStr.indexOf(".flv"));
    int32_t channel=channelStr.toInt();
    int32_t length=2;
    qDebug()<<channel;
    QByteArray header="HTTP/1.1 200 OK\r\nDate: Mon, 14 Nov 2011 02:49:45 GMT\r\nContent-Type: video/flv\r\nConnection: keep-alive\r\nExpires: Mon, 14 Nov 2011 02:49:45 GMT\r\nLast-Modified: Mon, 14 Nov 2011 02:49:45 GMT\r\nServer: SooonerServer/9.1.1.3844\r\nPragma: no-cache\r\nCache-Control: no-store, must-revalidate, post-check=0, pre-check=0, false\r\nContent-Length: -1\r\n\r\n";
    QTcpSocket *tcpSocket=new QTcpSocket();
    tcpSocket->connectToHost("127.0.0.1",1758);
    qDebug()<<"Host 127.0.0.1:1758 connected.";
    tcpSocket->write((char*)&length,sizeof(int32_t));
    tcpSocket->waitForBytesWritten(1000);
    tcpSocket->write((char*)&channel,sizeof(int32_t));
    tcpSocket->waitForBytesWritten(1000);
    while(tcpSocket->bytesAvailable()<(int)sizeof(int32_t))QCoreApplication::processEvents();
    tcpSocket->read((char*)&length,sizeof(int32_t));
    qDebug()<<"Header length:"<<length;
    QByteArray data(length,0);
    while(tcpSocket->bytesAvailable()<length)QCoreApplication::processEvents();
    tcpSocket->read(data.data(),length);
    tcpSocket->close();
    delete tcpSocket;
    header.append(data);
    udpReceiver=new QUdpReceiver(this->tcpSocket,channel,header);
}
