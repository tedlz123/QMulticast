#ifndef QHTTPSERVER_H
#define QHTTPSERVER_H

#include <QTcpServer>
#include <QApplication>
#include "qudpreceiver.h"

class QHttpServer : public QObject
{
    Q_OBJECT
private:
    QTcpServer *tcpServer;
    QTcpSocket *tcpSocket;
    QUdpReceiver *udpReceiver;
    QMutex mutex;

public:
    explicit QHttpServer(QObject *parent = 0);
    ~QHttpServer();

signals:

public slots:
    void accept();
    void restart();
};

#endif // QHTTPSERVER_H
