#include "qmulticast.h"

QMulticast::QMulticast(QWidget *parent) : QWidget(parent)
{
    trayIcon=new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon("icon.png"));
    trayIcon->setToolTip(QString("QMulticast"));
    QMenu *trayIconMenu=new QMenu(this);
    QAction *quit=new QAction(QString("Quit"),this);
    connect(quit,SIGNAL(triggered()),this,SLOT(quit()));
    trayIconMenu->addAction(quit);
    trayIcon->setContextMenu(trayIconMenu);
}

void QMulticast::start()
{
    trayIcon->show();
}

void QMulticast::quit()
{
    close();
}
