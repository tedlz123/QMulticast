#ifndef QMULTICAST_H
#define QMULTICAST_H

#include <QWidget>
#include <QSystemTrayIcon>
#include <QMenu>

class QMulticast : public QWidget
{
    Q_OBJECT
private:
    QSystemTrayIcon *trayIcon;

public:
    explicit QMulticast(QWidget *parent = 0);
    void start();

signals:

public slots:
    void quit();

};

#endif // QMULTICAST_H
