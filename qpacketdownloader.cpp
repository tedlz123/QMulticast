#include "qpacketdownloader.h"
#include "qpacketqueue.h"

QPacketDownloader::QPacketDownloader(QPacketQueue *queue, int32_t channel, int32_t index, QObject *parent) : QObject(parent), queue(queue), channel(channel), index(index)
{

}

QPacketDownloader::~QPacketDownloader()
{
    qDebug()<<"QPacketDownloader is deleted.";
}

void QPacketDownloader::downloadPacket()
{
    char data[65535];
    int32_t length=1;
    QTcpSocket *tcpSocket=new QTcpSocket();
    qDebug()<<channel<<index;
    tcpSocket->connectToHost("127.0.0.1",1758);
    tcpSocket->waitForConnected();
    tcpSocket->write((char*)&length,sizeof(int32_t));
    tcpSocket->waitForBytesWritten();
    tcpSocket->write((char*)&channel,sizeof(int32_t));
    tcpSocket->waitForBytesWritten();
    tcpSocket->write((char*)&index,sizeof(int32_t));
    tcpSocket->waitForBytesWritten();
    tcpSocket->waitForReadyRead(10000);
    while(tcpSocket->bytesAvailable()<(int)sizeof(int32_t))QCoreApplication::processEvents();
    tcpSocket->read((char*)&length,sizeof(int32_t));
    qDebug()<<"Packet length:"<<length;
    while(tcpSocket->bytesAvailable()<(int)sizeof(int32_t))QCoreApplication::processEvents();
    tcpSocket->read((char*)&index,sizeof(int32_t));
    while(tcpSocket->bytesAvailable()<(int)sizeof(int32_t))QCoreApplication::processEvents();
    tcpSocket->read((char*)&length,sizeof(int32_t));
    qDebug()<<"Packet length:"<<length;
    while(tcpSocket->bytesAvailable()<length)QCoreApplication::processEvents();
    tcpSocket->read(data,length);
    tcpSocket->disconnectFromHost();
    if(tcpSocket->state()!=QAbstractSocket::UnconnectedState)tcpSocket->waitForDisconnected();
    tcpSocket->close();
    delete tcpSocket;
    Packet *packet=new Packet();
    packet->index=index;
    packet->array=QByteArray(data,length);
    queue->addPacket(packet);
    deleteLater();
}
