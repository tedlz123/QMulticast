#ifndef QPACKETDOWNLOADER_H
#define QPACKETDOWNLOADER_H

#include <QApplication>
#include <QTcpSocket>

class QPacketQueue;

class QPacketDownloader : public QObject
{
    Q_OBJECT
private:
    QPacketQueue *queue;
    int32_t channel,index;

public:
    explicit QPacketDownloader(QPacketQueue *queue, int32_t channel, int32_t index, QObject *parent = 0);
    ~QPacketDownloader();

protected:

signals:

public slots:
    void downloadPacket();

};

#endif // QPACKETDOWNLOADER_H
