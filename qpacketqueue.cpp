#include "qpacketqueue.h"

QPacketQueue::QPacketQueue(QTcpSocket *tcpSocket, int32_t channel, QByteArray header, QObject *parent) : QObject(parent),size(1<<5),channel(channel),header(header),front(0),rear(0),max(0),min(0),firstIn(true),firstOut(true),queue(new Packet*[size]()),tcpSocket(tcpSocket)
{

}

QPacketQueue::~QPacketQueue()
{
    int i;
    const QList<QObject*> childrenList=children();
    for(i=0;i<childrenList.size();i++)
    {
        qDebug()<<childrenList[i];
        if(childrenList[i]!=NULL)
        {
            ((QThread*)childrenList[i])->exit();
            ((QThread*)childrenList[i])->wait();
            delete childrenList[i];
        }
    }
    if(queue!=NULL)
    {
        for(i=0;i<size;i++)
        {
            if(queue[i]!=NULL)delete queue[i];
        }
        delete []queue;
    }
}

void QPacketQueue::addPacket(Packet *packet)
{
    mutex.lock();
    if(firstIn)
    {
        max=packet->index;
        min=packet->index;
        firstIn=false;
    }
    int i,distmin=packet->index-min,distmax=packet->index-max;
    if(distmax>=0)
    {
        if(distmax>0)qDebug()<<"distmax ="<<distmax;
        for(i=0;i<=distmax;i++)
        {
            if(((rear+1)&(size-1))==front)
            {
                if(queue[front]!=NULL)
                {
                    if(firstOut)
                    {
                        tcpSocket->write(header);
                        firstOut=false;
                    }
                    tcpSocket->write(queue[front]->array);
                    delete queue[front];
                    queue[front]=NULL;
                }
                front=(front+1)&(size-1);
                min+=1;
            }
            if(i==distmax)
            {
                queue[rear&(size-1)]=packet;
            }
            else
            {
                if(distmax<4)
                {
                    QThread *thread=new QThread(this);
                    QPacketDownloader *downloader=new QPacketDownloader(this,channel,max);
                    downloader->moveToThread(thread);
                    QObject::connect(thread,SIGNAL(started()),downloader,SLOT(downloadPacket()));
                    QObject::connect(thread,SIGNAL(finished()),thread,SLOT(deleteLater()));
                    thread->start();
                }
            }
            rear=(rear+1)&(size-1);
            max+=1;
        }
    }
    else if(distmin>=0)
    {
        if(queue[(front+distmin)&(size-1)]!=NULL)
        {
            delete packet;
        }
        else
        {
            queue[(front+distmin)&(size-1)]=packet;
        }
    }
    mutex.unlock();
}
