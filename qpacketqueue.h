#ifndef QPACKETQUEUE_H
#define QPACKETQUEUE_H

#include <QTcpSocket>
#include <QByteArray>
#include <QThread>
#include <QMutex>
#include "qpacketdownloader.h"

struct Packet
{
    int32_t index;
    QByteArray array;
};

class QPacketQueue : public QObject
{
    Q_OBJECT
private:
    const int32_t size,channel;
    QByteArray header;
    int32_t front,rear,max,min;
    bool firstIn,firstOut;
    QMutex mutex;
    Packet **queue;
    QTcpSocket *tcpSocket;
public:
    explicit QPacketQueue(QTcpSocket *tcpSocket, int32_t channel, QByteArray header, QObject *parent = 0);
    ~QPacketQueue();

signals:

public slots:
    void addPacket(Packet *packet);
};

#endif // QPACKETQUEUE_H
