#include "qudpreceiver.h"

QUdpReceiver::QUdpReceiver(QTcpSocket *tcpSocket, int32_t channel, QByteArray header, QObject *parent) : QObject(parent), header(header), udpSocket(new QUdpSocket()), queue(new QPacketQueue(tcpSocket,channel,header))
{
    udpSocket->bind(QHostAddress("236.1.1.1"),20001+channel);
    if(udpSocket->joinMulticastGroup(QHostAddress("236.1.1.1")))
    {
        qDebug()<<"QUdpReceiver initialised.";
        QObject::connect(udpSocket,SIGNAL(readyRead()),this,SLOT(receivePacket()));
    }
    else
    {
        qDebug()<<"QUdpReceiver failed to initialise.";
    }
}

QUdpReceiver::~QUdpReceiver()
{
    if(queue!=NULL)
    {
        delete queue;
    }
    if(udpSocket!=NULL)
    {
        if(udpSocket->state()==QAbstractSocket::BoundState)udpSocket->leaveMulticastGroup(QHostAddress("236.1.1.1"));
        udpSocket->close();
        delete udpSocket;
    }
}

void QUdpReceiver::receivePacket()
{
    char data[65535];
    int length=udpSocket->readDatagram(data,65535);
    /*if((double)qrand()/(double)RAND_MAX<0.01)
    {
        qDebug()<<"Random packet discarding applied.";
        return;
    }*/
    Packet *packet=new Packet();
    packet->index=((u_int8_t)data[7]<<24)+((u_int8_t)data[6]<<16)+((u_int8_t)data[5]<<8)+(u_int8_t)data[4];
    packet->array=QByteArray(data+sizeof(int32_t)*3,length-sizeof(int32_t)*3);
    queue->addPacket(packet);
}
