#ifndef QUDPRECEIVER_H
#define QUDPRECEIVER_H

#include <QThread>
#include <QUdpSocket>
#include <QTcpSocket>
#include "qpacketqueue.h"

class QUdpReceiver : public QObject
{
    Q_OBJECT
private:
    QTcpSocket *tcpSocket;
    QByteArray header;
    QUdpSocket *udpSocket;
    QPacketQueue *queue;

public:
    explicit QUdpReceiver(QTcpSocket *tcpSocket, int32_t channel, QByteArray header, QObject *parent = 0);
    ~QUdpReceiver();

signals:

public slots:
    void receivePacket();
};

#endif // QUDPRECEIVER_H
