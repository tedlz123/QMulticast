#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <librtmp/rtmp.h>
#include <librtmp/amf.h>
#define CONTENT_LENGTH (1<<15)
#define SEGMENT_COUNT (1<<10)
#define THREAD_NUMBER 4
struct segment
{
    uint32_t length1;
    uint32_t seq;
    uint32_t length2;
    char content[CONTENT_LENGTH];
};
static char rtmp_urls[THREAD_NUMBER][51]=
{
    "rtmp://live.hkstv.hk.lxdns.com/live/hks",
    "rtmp://live.hkstv.hk.lxdns.com/live/hks",
    "rtmp://live.hkstv.hk.lxdns.com/live/hks",
    "rtmp://live.hkstv.hk.lxdns.com/live/hks"
};
static struct segment buffer[THREAD_NUMBER][SEGMENT_COUNT];
static char buffer_content[THREAD_NUMBER][CONTENT_LENGTH];
static char first[THREAD_NUMBER][CONTENT_LENGTH];
static uint32_t first_size[THREAD_NUMBER];
static pthread_t thread_id[THREAD_NUMBER];
struct segment segment_temp[THREAD_NUMBER];
static int thread_number[THREAD_NUMBER];
static pthread_mutex_t mutex;
static int server_sock;
int read_all(int sock,void *buffer,int size)
{
    int bytes=0,temp;
    while(bytes<size)
    {
        temp=read(sock,(char*)buffer+bytes,size-bytes);
        if(temp<0)return -1;else bytes+=temp;
    }
    return 0;
}
int write_all(int sock,const void *buffer,int size)
{
    int bytes=0,temp;
    while(bytes<size)
    {
        temp=write(sock,(char*)buffer+bytes,size-bytes);
        if(temp<0)return -1;else bytes+=temp;
    }
    return 0;
}
void do_exit(int dunno)
{
    close(server_sock);
    printf("The server has exited normally.\n");
    exit(0);
}
void *thread_rtmp(void *arg)
{
    int i,sock,length;uint32_t base=0;struct sockaddr_in address;RTMP *rtmp;
    printf("Thread %d is started.\n",*(int*)arg+1);
    for(i=0;i<SEGMENT_COUNT;i++)
    {
        buffer[*(int*)arg][i].length1=CONTENT_LENGTH+sizeof(uint32_t);
        buffer[*(int*)arg][i].seq=i;
        buffer[*(int*)arg][i].length2=CONTENT_LENGTH;
    }
    pthread_mutex_lock(&mutex);
    printf("Ready to allocate...\n");
    rtmp=RTMP_Alloc();
    RTMP_Init(rtmp);
    printf("RTMP pointer %p allocated.\n",rtmp);
    if(RTMP_SetupURL(rtmp,rtmp_urls[*(int*)arg])==FALSE)
    {
        printf("Thread %d: Cannot parse URL: %s\n",*(int*)arg+1,rtmp_urls[*(int*)arg]);
        pthread_mutex_unlock(&mutex);
        return ((void*)100);
    }
    sock=socket(AF_INET,SOCK_DGRAM,0);
    address.sin_family=AF_INET;
    address.sin_port=htons(20001+*(int*)arg);
    address.sin_addr.s_addr=inet_addr("236.1.1.1");
    rtmp->Link.timeout=300;
    rtmp->Link.lFlags|=RTMP_LF_LIVE;
    if(RTMP_Connect(rtmp,0)>=0)
    {
        printf("Thread %d: Connected to URL: %s\n",*(int*)arg+1,rtmp_urls[*(int*)arg]);
    }
    else
    {
        pthread_mutex_unlock(&mutex);
        return ((void*)100);
    }
    RTMP_ConnectStream(rtmp,0);
    length=RTMP_Read(rtmp,first[*(int*)arg],CONTENT_LENGTH);
    length+=RTMP_Read(rtmp,first[*(int*)arg]+length,CONTENT_LENGTH);
    first_size[*(int*)arg]=length;
    printf("%d %c%c%c\n",length,first[*(int*)arg][0],first[*(int*)arg][1],first[*(int*)arg][2]);
    pthread_mutex_unlock(&mutex);
    while(1)
    {
        pthread_mutex_lock(&mutex);
        length=RTMP_Read(rtmp,buffer_content[*(int*)arg],CONTENT_LENGTH);
        pthread_mutex_unlock(&mutex);
        segment_temp[*(int*)arg].length1=length+sizeof(uint32_t);segment_temp[*(int*)arg].seq=base;
        segment_temp[*(int*)arg].length2=length;memcpy(segment_temp[*(int*)arg].content,buffer_content[*(int*)arg],length);
        if(sendto(sock,segment_temp+(*(int*)arg),segment_temp[*(int*)arg].length1+(sizeof(uint32_t)<<1),0,(struct sockaddr*)&address,sizeof(struct sockaddr))<segment_temp[*(int*)arg].length1+sizeof(uint32_t))
        {
            printf("Warning, failed to send the multicast package Number %d.\n",base);
        }
        pthread_mutex_lock(&mutex);
        memcpy(buffer[*(int*)arg]+(base&(SEGMENT_COUNT-1)),segment_temp+(*(int*)arg),sizeof(struct segment));
        pthread_mutex_unlock(&mutex);
        base++;
    }
    return ((void*)100);
}
int main()
{
    int i,error;uint32_t request=0;
    struct sockaddr_in s_add,c_add;
    signal(SIGINT,do_exit);
    signal(SIGTERM,do_exit);
    pthread_mutex_init(&mutex,0);
    for(i=0;i<THREAD_NUMBER;i++)
    {
        thread_number[i]=i;error=pthread_create(thread_id+i,0,thread_rtmp,thread_number+i);
        if(error!=0)
        {
            printf("Error occured when creating Thread %d: %s\n",i+1,strerror(error));
            thread_id[i]=0;
        }
    }
    server_sock=socket(AF_INET,SOCK_STREAM,0);
    if(server_sock==-1)
    {
        printf("Socket error!\n");
        return -1;
    }
    memset(&s_add,0,sizeof(struct sockaddr_in));
    s_add.sin_family=AF_INET;
    s_add.sin_addr.s_addr=htonl(INADDR_ANY);
    s_add.sin_port=htons(1758);
    if(bind(server_sock,(struct sockaddr*)(&s_add),sizeof(struct sockaddr))==-1)
    {
        printf("Binding failure!\n");
        return -1;
    }
    if(listen(server_sock,5)==-1)
    {
        printf("Listening failure!\n");
        return -1;
    }
    fd_set fdset;
    FD_ZERO(&fdset);
    FD_SET(server_sock,&fdset);
    while(1)
    {
        fd_set fdread=fdset;
        int value=select(FD_SETSIZE,&fdread,0,0,0);
        if(value>0)
        {
            for(i=0;i<FD_SETSIZE;i++)
            {
                if(FD_ISSET(i,&fdread)==1&&i==server_sock)
                {
                    int sock,sin_size=sizeof(struct sockaddr_in);
                    sock=accept(server_sock,(struct sockaddr*)&c_add,(unsigned int*)&sin_size);
                    printf("Client %d connected.\n",sock);FD_SET(sock,&fdset);
                }
                else if(FD_ISSET(i,&fdread)==1)
                {
                    if(recv(i,&request,sizeof(uint32_t),MSG_PEEK)>=sizeof(uint32_t))
                    {
                        struct segment temp;int channel_id;
                        read_all(i,&request,sizeof(uint32_t));
                        printf("Command ID: %u\n",request);
                        if(request<2)
                        {
                            read_all(i,&request,sizeof(uint32_t));
                            printf("Channel ID: %u\n",request);
                            channel_id=request-1;
                            read_all(i,&request,sizeof(uint32_t));
                            printf("Packet ID: %u\n",request);
                            pthread_mutex_lock(&mutex);
                            memcpy(&temp,buffer[channel_id]+(request&(SEGMENT_COUNT-1)),sizeof(struct segment));
                            pthread_mutex_unlock(&mutex);
                            if(write_all(i,&temp,temp.length1+(sizeof(uint32_t)<<1))==-1)
                            {
                                printf("Writing data Number %d failure!\n",request);
                            }
                            else
                            {
                                printf("Writing data Number (%d:%ld) succeed!\n",request,temp.length1+(sizeof(uint32_t)<<1));
                            }
                        }
                        else
                        {
                            read_all(i,&request,sizeof(uint32_t));
                            printf("Channel ID: %u\n",request);
                            channel_id=request-1;
                            write_all(i,first_size+channel_id,sizeof(uint32_t));
                            if(write_all(i,first+channel_id,first_size[channel_id])==-1)
                            {
                                printf("Writing header for Channel %d failure!\n",request);
                            }
                            else
                            {
                                printf("Writing header for Channel %d succeed!\n",request);
                            }
                        }
                    }
                    else
                    {
                        printf("Connection of Client %d closed.\n",i);
                        FD_CLR(i,&fdset);close(i);
                    }
                }
            }
        }
        else
        {
            printf("Select error.\n");
            break;
        }
    }
    return 0;
}
